//Please replace with your Yandex API key. You don't need to change an endpoint.
const api_key = 'replace your api key';
const endpoint = 'https://translate.yandex.net/api/v1.5/tr.json/translate';



function callbackProc(param){
	result.value = "";
	let sourceText = source.value;
	if(param)
	{
		sourceText = param;
		source.value = sourceText;
	}

	let data = {'text': sourceText};
	let params = new URLSearchParams();
	Object.keys(data).forEach(key => params.append(key, data[key]));
	
    //If you want to automatically detect for a source language, only need 'to'. (ex.  lang=ja)
    //Replace with other language code as you like.
	let path = '?key=' + api_key + '&lang=en-ja';

	//Create a request object
	let req = new Request(uri, {
		method: 'POST',
		headers: {
		    'Content-Type': 'application/x-www-form-urlencoded;'
		},
		body: params
	});

	//Start to connect
	fetch(req).then(function(response){
	    if (response.ok) {
	        response.json().then(function(json){
				let jsonObj = JSON.stringify(json);
				//result.value = jsonObj;
				let resultObj = JSON.parse(jsonObj);
				result.value = resultObj.text[0];
	        });
	    }
	    else {
	    	throw new Error(`${response.status}: ${response.statusText}`);
	    }
	}).catch(function(error){
		result.value = error.message;
	});

}
