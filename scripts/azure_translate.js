//Please replace with your Azure API key. You don't need to change an endpoint.
const api_key = 'replace your api key';
const endpoint = 'https://api.cognitive.microsofttranslator.com';



function callbackProc(param){
	result.value = "";
	let sourceText = source.value;
	if(param)
	{
		sourceText = param;
		source.value = sourceText;
	}
	let obj = [{'Text':sourceText}];
	let body = JSON.stringify(obj);
    //if you want to automatically detect for a source language, no need 'from' parameter.
    //And regarding a 'to' parameter, replace with other language code as you like.
	let path = '/translate?api-version=3.0&from=en&to=ja&includeSentenceLength=true';
	let uri = endpoint + path;

	//Start ajax connection
	$.ajax({
		url: uri,
		type: 'POST',
		dataType: 'json',
		headers: {
			'Ocp-Apim-Subscription-Key':api_key,
		    'Content-Type': 'application/json;charset=UTF-8'
		  },
		data: body,
		timeout: 20000,
	})
	.done(function(data, dataType) {
		// Succeeded
		let jsonObj = JSON.stringify(data);
		//result.value = jsonObj;
		let resultObj = JSON.parse(jsonObj);
		result.value = resultObj[0].translations[0].text;
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		// Failed
		result.value = `${jqXHR.status}: ${textStatus} : ${errorThrown}`;
	});
}


