# Translation web page sample source code using rest api for "OCR de PawS" #

## Description ##
OCR de PawS is uwp app to detect text in an image by ocr and translate.

OCR de PawS has been published in the Microsoft Store.  https://www.microsoft.com/store/apps/9PFJX6P27QZ1

It is localized in English and Japanese.


This app has a feature to add-in for your translation web page.

If you develop or developed a translation web page using rest api, try to link your web page and the app.


---------------------------------
## Sample programs ##
There are two sample programs. one for Azure Rest API, another for  Yandex Rest API.

Each program is composed 3 files. (html, css, javascript)

As to asynchronous communicate, used jQuery and Fetch API. Please replace as you like.    

###  
![MyPage](https://symmetry-soft.com/software/paws/img/detail/UserWebDemoSmall.jpg)



### ###
## Deploy to App ##
Enter two elements id (source and result) and callback function name on translation service managing page in app.

Please see my web site for detail.(Currently, only in japanese)

https://symmetry-soft.com/software/paws/html/detail/user_web.htm    

###  
![MyPage](https://symmetry-soft.com/software/paws/img/detail/addin03Small.jpg)


